#!/bin/bash

export PACKER_CACHE_DIR=~/packer_cache
export PATH="${PATH}:../bin"

#echo $PATH

IMAGES=`find . -maxdepth 1 -type d | xargs | sed 's#./##g' | sed 's/\. //g'`

for img in $IMAGES; do

	echo $img
	cd $img
	VERSIONS=`find . -maxdepth 1 -type f -name "*.json" | xargs | sed 's/.json//g' | sed 's#./##g'`	
	for ver in $VERSIONS;
	do
		echo "-> $ver"
		if [[ -d $ver ]]; then
			rm -rf $ver
		fi
		time packer build $ver.json
	done
	cd - > /dev/null 
done 

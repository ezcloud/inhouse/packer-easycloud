#!/bin/bash

find . -name "output-*" -type d -print0 | xargs -0 /bin/rm -fr

#!/bin/bash

export PACKER_CACHE_DIR=~/packer_cache
export PATH="${PATH}:../bin"

QEMU=`which qemu-system-x86_64`

if [[ ! -x $QEMU ]]; then
	sudo apt-get install -y qemu-kvm
fi

BASE_DIR=`pwd`

echo "Building CentOS images..."
cd centos
packer build -var-file=centos6.7-x86_64.json centos.json
packer build -var-file=centos7.1.1503-x86_64.json centos.json
packer build -var-file=centos7.2.1511-x86_64.json centos.json
packer build -var-file=centos7.3.1611-x86_64.json centos.json

echo "Building Debian images..."
cd $BASE_DIR
cd debian
packer build -var-file=debian711.json debian.json
packer build -var-file=debian88.json debian.json

echo "Building Ubuntu images..."
cd $BASE_DIR
cd ubuntu
packer build -var-file=ubuntu1404.json ubuntu.json
packer build -var-file=ubuntu1604.json ubuntu.json
packer build -var-file=ubuntu1704.json ubuntu.json

cd $BASE_DIR
